﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using w60054.Services;
using w60054.Views;

namespace w60054
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new ItemDetailPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
