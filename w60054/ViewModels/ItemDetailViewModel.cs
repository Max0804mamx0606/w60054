﻿using System;

using w60054.Models;

namespace w60054.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.imie;
            Item = item;
        }
    }
}
