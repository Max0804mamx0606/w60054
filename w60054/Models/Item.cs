﻿using System;

namespace w60054.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string imie { get; set; }
        public string nazwisko { get; set; }
        public string plec { get; set; }
        public int wiek { get; set; }
        public string email { get; set; }
        public string zdjecie { get; set; }
    }
}